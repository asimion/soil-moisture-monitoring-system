# Soil Moisture Monitoring System

IoT ACES Lab Project

## Prerequisites

It is used Arduino IDE for development. So you need to install the eps32 board support in Arduino IDE. [Go!](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/) <br>
Library's which needs to be uploaded/imported :
   * [LiquidCrystal_I2C](https://www.arduinolibraries.info/libraries/liquid-crystal-i2-c)
   * [Adafruit_MQTT](https://www.arduinolibraries.info/libraries/adafruit-mqtt-library)
   * [ESP_Mail_Client](https://www.arduinolibraries.info/libraries/esp-mail-client)

## The Purpose

The purpose of this project is to have good irrigation management that will provide better crops or healthier plants.

## Implementation

### Soil Moisture Monitoring System - System Schematic
 As hardware components for the realization of this project I used:
<br>
   1. Development board ESP-WROOM-32
   2. Display LCD I2C
   3. Resistive Soil Moisture Sensor
   4. WiFi Router

![Soil Moisture Monitoring System - System Schematic]( SOILMOISTUREMONITORSYSTEM.jpeg "System Schematic")
<br>
<br>
### Soil Moisture Monitoring System - Software Schematic
![Soil Moisture Monitoring System - Software Schematic]( SOFTWARE_SCHEMATIC_Soil_Moisture_System.jpeg "Software Schematic")




