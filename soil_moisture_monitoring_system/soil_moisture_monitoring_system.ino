#include "time.h"
#include "LiquidCrystal_I2C.h"
#include "WiFi.h"
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include "ESP_Mail_Client.h"

/* CONFIG VARS LCD I2C */
#define LCD_COLUMNS 16
#define LCD_ROWS 2
#define ADDR_REG_LCD 0x27

/* CONFIG VARS SMTP GMAIL */
#define SMTP_HOST "smtp.gmail.com"
#define SMTP_PORT 465
// change the bottom line with your esp32 email addres
#define AUTHOR_EMAIL "YOUR_ESP_GMAIL_ACCOUNT@gmail.com"
// change the bottom line with the appropriate password
#define AUTHOR_PASSWORD "PASSWD_ESP_GMAIL_ACCOUNT"
// change the bottom line with the appropiate recipient gmail  
#define RECIPIENT_EMAIL "RECIPIENT@gmail.com" 

/* CONFIG VARS MQTT SERVER */
#define MQTT_SERV "io.adafruit.com"
#define MQTT_PORT 1883
//change the bottom line with your MQTT_NAME from io.adafruite.com
#define MQTT_NAME "YOUR_MQTT_NAME"
//change the bottom line with your MQTT_PASS from io.adafruite.com
#define MQTT_PASS "YOUR_MQTT_NAME"

/* CONFIG VARS SOIL MOISTURE SENSOR */
#define SSMPin 35  //Sensor Soil Moisture --> P35
#define MAX_SOIL_MOISTURE 100
#define MIN_SOIL_MOISTURE 0
#define DRY_THRESHOLD  40
#define ADC_MAX_VALUE_BITS 4095

/* CONFIG VARS NTP SERVER */
#define NTP_SERVER "ro.pool.ntp.org"
#define GMT_OFFSET_SEC 3600 * 2  //gmt +2 Bucharest
#define DAY_LIGHT_OFFSET_SEC 3600
#define PLANNED_EMAIL_HOUR "06:30"
#define ERROR_GETLOCALTIME "TIME ERR"

struct tm timeinfo;

/* WiFi */
//change the bottom line with your WIFI SSID
const char* mSSID = "your_wifi_ssid";
//change the bottom line with your WIFI PASS
const char* mPASSWD = "your_passwd";

SMTPSession smtp; // declare smtp session
ESP_Mail_Session session; // declare the session config data
SMTP_Message message; // declare the message class

WiFiClient mClient; //declare WiFi client

LiquidCrystal_I2C lcd(ADDR_REG_LCD, LCD_COLUMNS, LCD_ROWS);  
Adafruit_MQTT_Client mqtt(&mClient, MQTT_SERV, MQTT_PORT, MQTT_NAME, MQTT_PASS);
Adafruit_MQTT_Publish SoilMonitoring = Adafruit_MQTT_Publish(&mqtt, MQTT_NAME "/f/soil-moistrure-monitoring");

/* Callback function to get the Email sending status */
void smtpCallback(SMTP_Status status) {
  /* Print the current status */
  Serial.println(status.info());

  /* Print the sending result */
  if (status.success()){
    Serial.println("----------------");
    ESP_MAIL_PRINTF("Message sent success: %d\n", status.completedCount());
    ESP_MAIL_PRINTF("Message sent failled: %d\n", status.failedCount());
    Serial.println("----------------\n");
    struct tm dt;

    for (size_t i = 0; i < smtp.sendingResult.size(); i++){
      /* Get the result item */
      SMTP_Result result = smtp.sendingResult.getItem(i);
      time_t ts = (time_t)result.timestamp;
      localtime_r(&ts, &dt);

      ESP_MAIL_PRINTF("Message No: %d\n", i + 1);
      ESP_MAIL_PRINTF("Status: %s\n", result.completed ? "success" : "failed");
      ESP_MAIL_PRINTF("Date/Time: %d/%d/%d %d:%d:%d\n", dt.tm_year + 1900, dt.tm_mon + 1, dt.tm_mday, dt.tm_hour, dt.tm_min, dt.tm_sec);
      ESP_MAIL_PRINTF("Recipient: %s\n", result.recipients);
      ESP_MAIL_PRINTF("Subject: %s\n", result.subject);
    }
    Serial.println("----------------\n");
  }
}

void set_session_config(ESP_Mail_Session *session) {
  (*session).server.host_name = SMTP_HOST;
  (*session).server.port = SMTP_PORT;
  (*session).login.email = AUTHOR_EMAIL;
  (*session).login.password = AUTHOR_PASSWORD;
  (*session).login.user_domain = "";
}

void set_message_header(SMTP_Message *message) {
  (*message).sender.name = "ESP32 IOT";
  (*message).sender.email = AUTHOR_EMAIL;
  (*message).subject = "Warning Dry Soil";
  (*message).addRecipient("Andrei", RECIPIENT_EMAIL);  
}

void set_html_message(String html_msg) {
  message.html.content = html_msg.c_str();
  message.html.content = html_msg.c_str();
  message.text.charSet = "us-ascii";
  message.html.transfer_encoding = Content_Transfer_Encoding::enc_7bit;
}

void send_message(String moisture_percentage){
    String html_msg = "<div style=\"color:#2f4468;\"><h1>Moisture(%) : " + moisture_percentage + "</h1><p>- Sent from ESP board</p></div>";
    set_html_message(html_msg)  ;
      /* Connect to server with the session config */
    if (!smtp.connect(&session)) return;
      /* Start sending Email and close the session */
    if (!MailClient.sendMail(&smtp, &message)) Serial.println("Error sending Email, " + smtp.errorReason());  
}

void MQTT_connect() {
  int8_t ret;
  /* Stop if already connected. */
  if (mqtt.connected()) {
    return;
  }
  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    mqtt.disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
    if (retries == 0) {
      /* basically die and wait for WDT to reset me */
      while (1);
    }
  }
}

void try_connect_WiFi() {
  WiFi.begin(mSSID, mPASSWD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
}

float compute_moisture_percentage() {
  float SSMValue = analogRead(SSMPin);
  float moisturePercentage = map(SSMValue, 0, ADC_MAX_VALUE_BITS, MIN_SOIL_MOISTURE, MAX_SOIL_MOISTURE) ;
  String dataPercentage = String(moisturePercentage);
  Serial.println("SSMValue : " + String(SSMValue));
  Serial.println("Soli Moisture (%) : " + dataPercentage);
  return moisturePercentage;
}

void print_localtime_test() {
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "Test : %A, %B %d %Y %H:%M:%S");
}

String get_time_hour() {
  char time_hour[3];
  char time_minute[3];
  if(!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return ERROR_GETLOCALTIME;
  }
  strftime(time_hour, 3, "%H", &timeinfo);
  strftime(time_minute, 3, "%M", &timeinfo);
  String time_hour_str = String(time_hour);
  String time_minute_str = String(time_minute); 
  return time_hour_str + ":" + time_minute_str;
}

void setup() {
  Serial.begin(115200);
  delay(10);
  
  lcd.begin();//init LCD
  lcd.backlight();//turn on LCD backlight 
  lcd.setCursor(0,0);//set lcd cursor
  delay(10);
  
  Serial.println("Connecting to ");
  Serial.println(mSSID);
  String _ssid = String(mSSID);
  lcd.print("Connectig to " + _ssid);
  try_connect_WiFi();//try to connect WiFi
  
  lcd.clear();
  lcd.print("WiFi connected");
  Serial.println("");
  Serial.println("WiFi connected");
  lcd.clear();

  /* Init and get the time */
  configTime(GMT_OFFSET_SEC, DAY_LIGHT_OFFSET_SEC, NTP_SERVER); 
  print_localtime_test();
  
  /*SMTP EMAIL SETUP*/
  smtp.debug(1); // basic debug on Serial PORT
  smtp.callback(smtpCallback); // set the callback function to get the sendig results
  /* SMTP session & message SETUP */
  set_session_config(&session); // set the session config
  set_message_header(&message); // set the message headers
}

void loop() {
  lcd.print("SoilMoisture-MONITOR");
 
  /*  perform compute moisture percentage */
  float moisturePercentage = compute_moisture_percentage();
  String dataPercentage = String(moisturePercentage);
  lcd.setCursor(0,1);
  lcd.print("Moisture(%): " + dataPercentage);
 
  /* perform connect to io.adafruit.com */
  MQTT_connect(); 
  
  /* perform publish data */
  if (! SoilMonitoring.publish(moisturePercentage)){                     
    delay(5000);   
  }
  
  delay(40000);

  /* perform sendig e-mail at PLANNED_EMAIL_HOUR */
  String current_hour = get_time_hour();
  Serial.println(current_hour);
  if(/*moisturePercentage <= DRY_THRESHOLD &&*/ current_hour == PLANNED_EMAIL_HOUR) {
    send_message(dataPercentage);
  } else {
    delay(5000);
  }

  lcd.clear();
}
